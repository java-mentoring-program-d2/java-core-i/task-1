package vehicles;

public abstract class Vehicle implements Comparable<Vehicle> {
    private final int mass;
    private final int maxSpeed;
    private final int capacity;

    private final long cost;
    private final long fuelCostPerMile;

    public Vehicle(int mass, int maxSpeed, int capacity) {
        this.mass = mass;
        this.maxSpeed = maxSpeed;
        this.capacity = capacity;
        this.cost = countAndGetBusCost();
        this.fuelCostPerMile = countAndGetFuelCostPerMile();
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "mass=" + mass +
                ", maxSpeed=" + maxSpeed +
                ", capacity=" + capacity +
                ", cost=" + cost +
                ", fuelCostPerMile=" + fuelCostPerMile +
                '}';
    }

    @Override
    public final int compareTo(Vehicle o) {
        return (int) (o.getFuelCostPerMile() - this.getFuelCostPerMile());
    }

    abstract EnergyType getType();

    private long countAndGetBusCost() {
        return Math.round(getType().getEngineCost() * countAndGetFuelCostPerMile() * capacityCoefficient()
                * maxSpeedCoefficient() * massCoefficient());
    }

    private long countAndGetFuelCostPerMile() {
        return Math.round(getType().getCostPerMile() * (3.0 - massCoefficient()));
    }

    public final int getMass() {
        return mass;
    }

    public final int getMaxSpeed() {
        return maxSpeed;
    }

    public final int getCapacity() {
        return capacity;
    }

    public final long getCost() {
        return cost;
    }

    public final long getFuelCostPerMile() {
        return fuelCostPerMile;
    }

    private double capacityCoefficient() {
        if (capacity <= 50) {
            return 1.0;
        } else if (capacity <= 100) {
            return 1 + (capacity - 50) / 50.0;
        } else {
            return 2.0;
        }
    }

    private double maxSpeedCoefficient() {
        if (maxSpeed <= 60) {
            return 1.0;
        } else if (maxSpeed <= 80) {
            return 1 + (maxSpeed - 60) / 40.0;
        } else {
            return 1.5;
        }
    }

    private double massCoefficient() {
        if (mass <= 10000) {
            return 2.0;
        } else if (mass <= 25000) {
            return 1 + (mass - 10000) / 15000.0;
        } else {
            return 1.0;
        }
    }
}
